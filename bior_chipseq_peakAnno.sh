####
# Annotation peaks in batch
####

## input file list
filelist=$1

## run homer peak annotation
while read peakfile; do

	#### peak annotation
	if [ ! -e $peakfile.anno ]
	then
	    annotatePeaks.pl $peakfile hg19 > $peakfile.anno
	else
    echo "(*_*) <$peakfile.anno> already exists...Skip~~"
	fi

done < $filelist


