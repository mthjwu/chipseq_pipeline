####
# Downsample from rmdup bam file
####

#### set path of the pipeline
bdir=`dirname $0`


#### get pars
samplename=$1
randomN=$2

#### total mapped reads
readN=`grep -E "mapped \(" $samplename.rmdup.mapstat |sed 's/ + 0 mapped.*$//g'`

#### prepare parameters for subsampling
foldN=`awk -v randomN=$randomN -v readN=$readN 'BEGIN {print int(randomN/readN)}'`
fracN=`awk -v randomN=$randomN -v readN=$readN 'BEGIN {print randomN/readN}'`
fracN=`awk -v foldN=$foldN -v fracN=$fracN 'BEGIN {print 100*(fracN-foldN)}'`

#### downsample to n
if [ ! -e $samplename.rmdup.ds$randomN.bed ]
then
	## bam2bed
	macs2 randsample -t $samplename.rmdup.bam -p 100.0 --seed 123 -o $samplename.rmdup.bed

	## remove bed lines
	awk '$2>0' $samplename.rmdup.bed > $$; mv $$ $samplename.rmdup.bed

	## subsample
	for i in `seq 1 $foldN`;
	do
		cat $samplename.rmdup.bed >> $samplename.rmdup.ds$randomN.presort.bed
	done
	macs2 randsample -t $samplename.rmdup.bed -p $fracN --seed 123 >> $samplename.rmdup.ds$randomN.presort.bed

	## sort bed file
	sortBed -i $samplename.rmdup.ds$randomN.presort.bed > $samplename.rmdup.ds$randomN.bed

	## clean tmp files
	rm $samplename.rmdup.bed $samplename.rmdup.ds$randomN.presort.bed
else
	echo "(*_*) <$samplename.rmdup.ds$randomN.bed> already exists...Skip~~"
fi
