#!/bin/bash
####
# union peaks and find summits, then plot
####

## input files
bdir=`dirname $0`
flist=$1
out=$2

## renew the ngsplot and diffcomp config file
if [ -e $out.ngsplot.config ]
then
	rm $out.ngsplot.config
fi
if [ -e $out.diffcomp.config ]
then
	rm $out.diffcomp.config
fi

## parsing the flist
while read line; do
	file=($line)
	sname=${file[1]/_summits.bed/}
	if [ ! -e $sname.npeak.summit ]
	then
		paste ${file[0]} ${file[1]} |cut -f1-4,12 > $sname.npeak.summit
	else
		echo "(*_*) <$sname.npeak.summit> already exists...Skip~~"
	fi
	pslist+=($sname.npeak.summit)
	## write a config file for ngsplot
	echo -e "$out.${sname}.siteproBW.nps_dump.txt\t${file[2]}" >> $out.ngsplot.config
	## write a config file for union read count plot
	echo -e "$out.npeak.${sname}.rmdup.nlzcounts\t${file[2]}" >> $out.diffcomp.config
done < $flist

## peak union
if [ ! -e $out.npeak.summit ]
then
	## put all into one bed and sort
	bedops -u ${pslist[@]} > $out.all
	## union peaks
	bedtools merge -n -nms -scores mean -i $out.all > $out.npeak
	## change to summit mid
	awk '{print $1"\t"int($5)"\t"int($5)+1"\t"$4"\t"$6}' $out.npeak |bedtools sort -i > $out.npeak.summit
	## get the peak summit in the narrow peak bed
	awk '{print $1"\t"$2"\t"$3"\t"$4"\t"int($5)"\t"$6}' $out.npeak > $$; mv $$ $out.npeak
else
	echo "(*_*) <$out.npeak.summit> already exists...Skip~~"
fi

## union peak summits profile
for i in ${pslist[@]}
do
	sname=${i/.npeak.summit/}
	if [ ! -e $out.${sname}.siteproBW.nps_dump.txt ]
	then
    	siteproBW -w ${sname}_treat_pileup.bw -b $out.npeak.summit --span=3000 --dump --name=$out.${sname}.siteproBW.nps -l $out.${sname}.siteproBW.nps
	else
		echo "(*_*) <$out.${sname}.siteproBW.nps_dump.txt> already exists...Skip~~"
	fi
done

## ngsplot
if [ ! -e $out.ngsplot.heatmap.pdf ]
then
	bior_chipseq_ngsplot.sh $out.ngsplot.config $out.ngsplot 0
else
	echo "(*_*) <$out.ngsplot.heatmap.pdf> already exists...Skip~~"
fi


## read counts in union peak regions
for i in ${pslist[@]}
do
	sname=${i/.npeak.summit/}
	if [ ! -e $out.npeak.${sname}.rmdup.nlzcounts ]
	then
    	bior_chipseq_bedcounts.sh $out.npeak ${sname}.rmdup.bam
	else
		echo "(*_*) <$out.npeak.${sname}.rmdup.nlzcounts> already exists...Skip~~"
	fi
done


## pair scatter plot comparing samples
if [ ! -e $out.diffcomp.boxplot.pdf ]
then
	Rscript $bdir/bior_chipseq_diffcomp.R $out.diffcomp.config $out.diffcomp
else
	echo "(*_*) <$out.diffcomp.pdf> already exists...Skip~~"
fi




