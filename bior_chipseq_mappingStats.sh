#####
# output formated mapping stats for chipseq
#####

#### input
prefix=$1


#### parse data and output
cat <(echo -e "FileName\tToal\tMapped\tRmdupTotal\tRmdupMapped") \
    <(paste <(find . -maxdepth 1 -name "$prefix*.rmdup.mapstat" |sort |sed 's/\.\///g' |sed 's/rmdup.mapstat/fastq.gz/g') \
      <(find . -maxdepth 1 -name "$prefix*mapstat" |grep -v "rmdup.mapstat" |sort |xargs grep " reads; of these:" |sed 's/ reads; of these://g' |sed 's/.*://g') \
      <(find . -maxdepth 1 -name "$prefix*mapstat" |grep -v "rmdup.mapstat" |sort |xargs grep "aligned 0 times" |sed 's/(.*//g' |sed 's/.*://g' |sed 's/ *//g') \
      <(find . -maxdepth 1 -name "$prefix*.rmdup.mapstat" |sort |xargs paste |grep "in total " |sed 's/ + 0 in total (QC-passed reads + QC-failed reads)//g' |sed 's/ + 0 mapped[^\t]*//g' |sed 's/\t/\n/g') \
      <(find . -maxdepth 1 -name "$prefix*.rmdup.mapstat" |sort |xargs paste |grep -E "mapped \(" |sed 's/ + 0 in total (QC-passed reads + QC-failed reads)//g' |sed 's/ + 0 mapped[^\t]*//g' |sed 's/\t/\n/g') \
      |awk '{print $1"\t"$2"\t"$2-$3"\t"$4"\t"$5}')


