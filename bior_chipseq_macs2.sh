#!/bin/bash
####
# Just run macs2 peak calling to test different parameter
####

#### set path of the pipeline
bdir=`dirname $0`
ceas=$bdir/CEAS/bin

#### useage
red='\033[0;31m'
blue='\033[1;34m'
green='\033[1;32m'
NC='\033[0m' # No Color
options_help="
${red}Command line${NC}: ${green}
    bior_chipseq_macs2.sh [options] [-t <test_fastq_file>] [-c <control_fastq_file>] [-g <genome annotation version: hg19_refseq,hg19_gencode19,mm10_gencodeM3>]

${red}Options${NC}: ${green}
	-q | --qvalue:     q-value cutoff for macs2
	-p | --pvalue:     p-value cutoff for macs2 (if this is set, qvalue will be not used)
	-B | --broad:      Broad peak calling with Macs2
	-N | --nobdg:      Don't allow macs2 to output normalized bdg file
	-h | --help:       help
${NC}
"
usage () {
	echo -e "\n${red}Usage${NC}: ${green}`basename $0` -h for help${NC}"
	echo -e "$options_help"
	exit 1
}


#### set defalt arguments
broad_peak=0
macs2_pileup=1
macs2_qval=0.05
macs2_pval=-1

# read the options
TEMP=`getopt -o t:c:g:q:p:BN --long test-file:,control-file:,genome:,qvalue:,pvalue:,broad,nobdg -n 'bior_chipseq_macs2.sh' -- "$@"`
eval set -- "$TEMP"


#### passing arguments
while :
do
    case "$1" in
    	-t | --test-file)
    			case "$2" in
    					"") shift 2;;
      				    *) test_file="$2"; shift 2;;
      		esac ;;
    	-c | --control-file)
    			case "$2" in
    					"") shift 2;;
    					*) control_file="$2"; shift 2;;
    			esac;;
	  	-g | --genome)
	  			case "$2" in
    					"") shift 2;;
    					*) annotation="$2"; shift 2;;
    			esac;;
		-q | --qvalue)
				case "$2" in
						"") shift 2;;
						*) macs2_qval="$2"; shift 2;;
				esac;;
		-p | --pvalue)
				case "$2" in
						"") shift 2;;
						*) macs2_pval="$2"; shift 2;;
				esac;;
	  	-B | --broad)
	  			broad_peak=1
	  			shift
	  			;;
		-N | --nobdg)
				macs2_pileup=0
				shift
				;;
      -h | --help)
	  			usage
	  			exit 0
	  			;;
      --) # End of all options
	  			shift
	  			break
	  			;;
      -*)
	  			echo "Error: Unknown option: $1" >&2
	  			exit 1
	  			;;
      *)  # No more options
      		exit 1
	  			;;
    esac
done


#### judge parameters
if [ -z $test_file ] || [ -z $control_file ]
then
	usage
	exit 1
fi


#### set all parameters from command line options
testname=${test_file/".fastq.gz"/}
testname=${testname/".fq.gz"/}
controlname=${control_file/".fastq.gz"/}
controlname=${controlname/".fq.gz"/}


#### peak calling
if [ $macs2_pval == -1 ]
then
	macs2_callpeak_parP="-q $macs2_qval"
	macs2_callpeak_suffix="q$macs2_qval"
else
	macs2_callpeak_parP="-p $macs2_pval"
	macs2_callpeak_suffix="p$macs2_pval"
fi
if [ $macs2_pileup == 1 ]
then
	macs2_callpeak_par1="--SPMR -B --keep-dup 1 --extsize=146 --nomodel -g hs"
	macs2_callpeak_par2="--SPMR -B --keep-dup 1 --extsize=146 --nomodel -g hs --broad"
else
	macs2_callpeak_par1="--keep-dup 1 --extsize=146 --nomodel -g hs"
	macs2_callpeak_par2="--keep-dup 1 --extsize=146 --nomodel -g hs --broad"
fi
if [ $broad_peak == 0 ]
then
	macs2 callpeak $macs2_callpeak_par1 $macs2_callpeak_parP -t $testname.rmdup.bam -c $controlname.rmdup.bam -n $testname.$macs2_callpeak_suffix
else
	macs2 callpeak $macs2_callpeak_par2 $macs2_callpeak_parP -t $testname.rmdup.bam -c $controlname.rmdup.bam -n $testname.$macs2_callpeak_suffix.broad
fi









