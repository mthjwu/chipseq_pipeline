####
# ngsplot using bior_chipseq_ngsplot.R
####

## set path of the pipeline
bdir=`dirname $0`
config=$1
outfile=$2
summitanno=$3

## run bior_chipseq_ngsplot.R
if [ ! -e $outfile.aveprof.pdf ]
then
	Rscript $bdir/bior_chipseq_ngsplot.R $config $outfile $summitanno
else
	echo "(*_*) <$outfile.aveprof.pdf> already exists...Skip~~" 
fi


