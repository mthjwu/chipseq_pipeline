#!/bin/bash
####
# union peaks and find summits, then plot
####

## input files
out=$1
shift
prefixlist=$@
echo "Prefix list: $prefixlist"
echo "Out prefix: $out"

## set array
prefixarray=($prefixlist)
for i in ${prefixarray[@]}
do
	paste ${i}_peaks.narrowPeak ${i}_summits.bed |cut -f1-4,12 > ${i}.npeak.summit
done

## print to check
peaklist=${prefixarray[@]/%/_peaks.narrowPeak}
summitlist=${prefixarray[@]/%/_summits.bed}
pslist=${prefixarray[@]/%/.npeak.summit}
#echo "Peak file list: $peaklist"
#echo "Summit file list: $summitlist"
#echo "Peak summit file list: $pslist"

## peak union
if [ ! -e $out.npeak.summit ]
then
	## put all into one bed and sort
	bedops -u $pslist > $out.all
	## union peaks
	bedtools merge -n -nms -scores mean -i $out.all > $out.npeak
	## change to summit mid
	awk '{print $1"\t"int($5)"\t"int($5)+1"\t"$4"\t"$6}' $out.npeak |bedtools sort -i > $out.npeak.summit
else
	echo "(*_*) <$out.npeak.summit> already exists...Skip~~"
fi

## union peak summits profile
for i in ${prefixarray[@]}
do
	if [ ! -e $out.${i}.siteproBW.nps_dump.txt ]
	then
    	siteproBW -w ${i}_treat_pileup.bw -b ${i}.npeak.summit --dump --name=$out.${i}.siteproBW.nps -l $out.${i}.siteproBW.nps
	else
		echo "(*_*) <$out.${i}.siteproBW.nps_dump.txt> already exists...Skip~~"
	fi
done


