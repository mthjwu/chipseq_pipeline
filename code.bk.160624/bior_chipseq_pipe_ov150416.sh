#!/bin/bash
####
# chipseq analysis pipeline
####

#### set path of the pipeline
bdir=`dirname $0`

#### useage
red='\033[0;31m'
blue='\033[1;34m'
green='\033[1;32m'
NC='\033[0m' # No Color
options_help="
${red}Command line${NC}: ${green}
    bior_chipseq_pipe.sh [options] [-t <test_fastq_file>] [-c <control_fastq_file>] [-g <genome annotation version: hg19_refseq,hg19_gencode19,mm10_gencodeM3>]

${red}Options${NC}: ${green}   
    -B | --broad:      Broad peak calling with Macs2
    -h | --help:       help
${NC}
"
usage () {
	echo -e "\n${red}Usage${NC}: ${green}`basename $0` -h for help${NC}"
	echo -e "$options_help"
	exit 1
}


#### set defalt arguments
broad_peak=0


# read the options
TEMP=`getopt -o t:c:g:B --long test-file:,control-file:,genome:,broad -n 'bior_chipseq_pipe.sh' -- "$@"`
eval set -- "$TEMP"


#### passing arguments
while :
do
    case "$1" in
    	-t | --test-file)
    			case "$2" in
    					"") shift 2;;
      				*) test_file="$2"; shift 2;;
      		esac ;;
    	-c | --control-file)
    			case "$2" in
    					"") shift 2;;
    					*) control_file="$2"; shift 2;;
    			esac;;
	  	-g | --genome)
	  			case "$2" in
    					"") shift 2;;
    					*) annotation="$2"; shift 2;;
    			esac;;
	  	-B | --broad)
	  			broad_peak=1
	  			shift
	  			;;
      -h | --help)
	  			usage
	  			exit 0
	  			;;
      --) # End of all options
	  			shift
	  			break
	  			;;
      -*)
	  			echo "Error: Unknown option: $1" >&2
	  			exit 1
	  			;;
      *)  # No more options
      		exit 1
	  			;;
    esac
done


#### judge parameters
if [ -z $test_file ] || [ -z $control_file ] || [ -z $annotation ]
then
	usage
	exit 1
fi


#### set all parameters from command line options
sampledir=`dirname $test_file`
testname=`basename $test_file ".fq.gz"`
testname=`basename $testname ".fastq.gz"`
controlname=`basename $control_file ".fq.gz"`
controlname=`basename $controlname ".fastq.gz"`


#### set genome version and annotation version
source $bdir/bior_chipseq_set_genome_annotation.sh


#### mapping with bowtie2
bior_chipseq_map_bowtie2.sh $test_file $annotation
bior_chipseq_map_bowtie2.sh $control_file $annotation


#### peak calling
if [ ! -e ${testname}_peaks.xls ]
then
		macs2_callpeak_par1="--SPMR -B --keep-dup 1 --extsize=146 --nomodel -g hs -q 0.01"
		macs2_callpeak_par2="--SPMR -B --keep-dup 1 --extsize=146 --nomodel -g hs -q 0.01 --borad"
		if [ $broad_peak == 0 ]
		then
				macs2 callpeak $macs2_callpeak_par1 -t $testname.rmdup.bam -c $controlname.rmdup.bam -n $testname
		else
				macs2 callpeak $macs2_callpeak_par2 -t $testname.rmdup.bam -c $controlname.rmdup.bam -n $testname
		fi
else
		echo "(*_*) <${testname}_peaks.xls> already exists...Skip~~"
fi


#### bedgraph2bigwiggle
if [ ! -e ${testname}_treat_pileup.bw ]
then
		bedClip ${testname}_treat_pileup.bdg $chromlen $$
		bedGraphToBigWig $$ $chromlen ${testname}_treat_pileup.bw
		rm $$
		bedClip ${testname}_control_lambda.bdg $chromlen $$
		bedGraphToBigWig $$ $chromlen ${testname}_control_lambda.bw
		rm $$
else
		echo "(*_*) <${testname}_treat_pileup.bw> already exists...Skip~~"
fi

#### peak annotation
#annotatePeaks.pl ${testname}_peaks.narrowPeak hg19 > ${testname}_peaks.narrowPeak.anno


