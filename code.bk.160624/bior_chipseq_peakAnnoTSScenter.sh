####
# Annotation peaks
####

#### set path of the pipeline
bdir=`dirname $0`

#### input file
peakfile=$1

#### peak annotation by TSS position
if [ ! -e $peakfile.annotsscenter ]
then
    bedtools closest -D b -t first -a $bdir/ref/hg19/hg19_refgene_tss.bed -b $peakfile > $peakfile.annotsscenter
else
	echo "(*_*) <$peakfile.annotsscenter> already exists...Skip~~"
fi


