####
# Downsample from rmdup bam file
####

#### set path of the pipeline
bdir=`dirname $0`


#### get pars
samplename=$1
randomN=$2


#### downsample to n
if [ ! -e $samplename.rmdup.ds$randomN.bed ]
then
	macs2 randsample -t $samplename.rmdup.bam -n $randomN --seed 123 -o $samplename.rmdup.ds$randomN.bed
else
	echo "(*_*) <$samplename.rmdup.ds$randomN.bed> already exists...Skip~~"
fi
