#!/bin/bash
####
# union peaks, calculate normalized signal for each sample, calculate peak coverage (bp) for each sample in each union peak
####

## input files
bdir=`dirname $0`
flist=$1
out=$2

## parsing the flist
while read line; do
	file=($line)
	pslist+=(${file[0]})
	snlist+=(${file[1]})
done < $flist

echo ${pslist[@]}

## peak union
if [ ! -e $out.upeak ]
then
	## put all into one bed and sort
	bedops -u ${pslist[@]} > $out.all
	## union peaks
	bedtools merge -n -nms -scores mean -i $out.all > $out.upeak
else
	echo "(*_*) <$out.upeak> already exists...Skip~~"
fi

## annotation peaks by TSS
if [ ! -e $out.upeak.annotss ]
then
	bior_chipseq_peakAnnoTSS.sh $out.upeak
else
	echo "(*_*) <$out.upeak.annotss> already exists...Skip~~"
fi

## read counts of union peak regions in each sample
for i in ${!pslist[@]}
do
	sname=${snlist[$i]}
	if [ ! -e $out.upeak.${sname}.rmdup.nlzcounts ]
	then
    	bior_chipseq_bedcounts.sh $out.upeak ${sname}.rmdup.bam
	else
		echo "(*_*) <$out.upeak.${sname}.rmdup.nlzcounts> already exists...Skip~~"
	fi
done

## base coverage of union peak regions in each sample
for i in ${!pslist[@]}
do
	sname=${snlist[$i]}
	pname=${pslist[$i]}
	if [ ! -e $out.upeak.${sname}.rmdup.basecov ]
	then
    	bedtools intersect -wao -a $out.upeak -b $pname |awk 'NR==FNR {a[$1"."$2"."$3]+=$NF} NR>FNR {print $0"\t"a[$1"."$2"."$3]}' - $out.upeak > $out.upeak.${sname}.rmdup.basecov
	else
		echo "(*_*) <$out.upeak.${sname}.rmdup.basecov> already exists...Skip~~"
	fi
done









