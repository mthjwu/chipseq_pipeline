####
# Annotation peaks
####

#### set path of the pipeline
bdir=`dirname $0`

#### input file
peakfile=$1

#### peak annotation by TSS position
if [ ! -e $peakfile.annotss ]
then
    bedtools closest -D b -t first -a $peakfile -b $bdir/ref/hg19/hg19_refgene_tss.bed > $peakfile.annotss
else
	echo "(*_*) <$peakfile.annotss> already exists...Skip~~"
fi


