#!/bin/bash

#### set path of the pipeline
bdir=`dirname $0`

#### receive all parameters from command line
if [ $# == 2 ]
then
	type=SE
	samplefile=$1
	samplename=${samplefile/".fastq.gz"/}
	samplename=${samplename/".fq.gz"/}
	annotation=$2
elif [ $# == 3 ]
then
	type=PE
	samplefile="$1 $2"
	samplefile1=$1
	samplefile2=$2
	samplename=${samplefile1/"_R1.fastq.gz"/}
	samplename=${samplename/"_R1.fq.gz"/}
	annotation=$3
else
	echo "Wrong input>>>>>"
fi

#### set bowtie2 parameters
nprocess=16
bowtie2_par="-p $nprocess"

#### set genome version and annotation version
source $bdir/bior_chipseq_set_genome_annotation.sh

#### run bowtie2 on the fastq files
if [ ! -e $samplename.bam ] && [ ! -e $samplename.rmdup.bam ]
then
	if [ $type == "SE" ]
	then
		## qc for fastq file: cutadapt
		if [ ! -e $samplefile.cutadapt.fq.gz ]
		then
			bior_chipseq_cutadapt.sh $samplefile &> $samplename.cutadapt.log
		fi
		bowtie2 $bowtie2_par -x $index -U $samplefile.cutadapt.fq.gz -S $samplename.sam &> $samplename.mapstat
		#rm $samplefile.cutadapt.fq.gz
		#bowtie2 $bowtie2_par -x $index -U $samplefile -S $samplename.sam &> $samplename.mapstat
	else
		bowtie2 $bowtie2_par -x $index -1 $samplefile1 -2 $samplefile2 -S $samplename.sam &> $samplename.mapstat
	fi

	echo "Start generate $samplename.bam ..."
	samtools view -bS $samplename.sam > $samplename.presort.bam
	#rm $samplename.sam
	samtools sort $samplename.presort.bam -o $samplename.bam
	#java -jar $bdir/picard.jar ReorderSam I=$samplename.presort.bam O=$samplename.bam R=$chrom VALIDATION_STRINGENCY=LENIENT QUIET=true
	rm $samplename.presort.bam
else
	echo "### <$samplename.bam> already exists...Skip!"
fi

#### run some bam transformation and quanlity control
if [ ! -e $samplename.rmdup.bam ]
then
	java -jar $bdir/picard.jar MarkDuplicates I=$samplename.bam O=$samplename.rmdup.bam REMOVE_DUPLICATES=TRUE METRICS_FILE=dup.txt VALIDATION_STRINGENCY=LENIENT QUIET=true &> $samplename.rmdup.log
	samtools index $samplename.rmdup.bam
	samtools flagstat $samplename.rmdup.bam > $samplename.rmdup.mapstat
else
	echo "### <$samplename.rmdup.bam> already exists...Skip!"
fi

#### fastqc
if [ ! -e ${samplename}_fastqc.zip ] && [ ! -e ${samplename}_R1.fq_fastqc.zip ]
then
	echo "Start fastqc ..."
	fastqc $samplefile
else
	echo "### <${samplename}_fastqc.zip> already exists...Skip!"
fi


