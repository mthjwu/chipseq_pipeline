#!/bin/bash
####
# Count reads of each bed region from bam file, and normalize
####

## input files
bed=$1
bedprefix=${bed/.bed/}
bam=$2
bamdir=`dirname $bam`
bamprefix=`basename $bam ".bam"`


## get index of bam file
if [ ! -e $bam.bai ]
then
    samtools index $bam
else
    echo "(*_*) <$bam.bai> already exists...Skip~~"
fi

## raw read counts
bedtools multicov -bams $bam -bed $bed > $bedprefix.$bamprefix.rawcounts

## get stat of bam file
if [ ! -e $bamdir/$bamprefix.mapstat ]
then
    samtools flagstat $bam > $bamdir/$bamprefix.mapstat
else
    echo "(*_*) <$bamdir/$bamprefix.mapstat> already exists...Skip~~"
fi

## get total mapped reads
total=`awk 'NR==3' $bamdir/$bamprefix.mapstat |cut -d' ' -f1`

## normalize read counts
awk -v tot=$total '{print $0"\t"int($NF*1000000000/tot+0.5)/1000}' $bedprefix.$bamprefix.rawcounts > $bedprefix.$bamprefix.nlzcounts

## clean tmp data
rm $bedprefix.$bamprefix.rawcounts