#!/bin/bash
####
# chipseq analysis pipeline
####

#### set path of the pipeline
bdir=`dirname $0`
ceas=$bdir/CEAS/bin

#### useage
red='\033[0;31m'
blue='\033[1;34m'
green='\033[1;32m'
NC='\033[0m' # No Color
options_help="
${red}Command line${NC}: ${green}
    bior_chipseq_pipe.sh [options] [-t <test_fastq_file>] [-c <control_fastq_file>] [-g <genome annotation version: hg19_refseq,hg19_gencode19,mm10_gencodeM3>]

${red}Options${NC}: ${green}
	-q | --qvalue:             q-value cutoff for macs2
	-p | --pvalue:             p-value cutoff for macs2 (if this is set, qvalue will be not used)
	-B | --broad:              Broad peak calling with Macs2
	-N | --nobdg:              Don't allow macs2 to output normalized bdg file
	-M | --onlymapping:        Only do preprocessing and mapping
	-d | --downsampleN:        Down sample bam file to N tags for both test and input, if -D isn't set (default don't down sample)
	-D | --downsampleNinput:   Down sample bam file to N tags for input sample (default don't down sample)
	-A | --nocutadapt:         Don't use cutadapt to remove adaptor sequences from the raw reads
	-h | --help:               help

For pair-end data: bior_chipseq_pipe.sh -t \"chip_R1.fastq.gz chip_R2.fastq.gz\" -c \"input_R1.fastq.gz input_R2.fastq.gz\" -g hg19_refseq
${NC}
"
usage () {
	echo -e "\n${red}Usage${NC}: ${green}`basename $0` -h for help${NC}"
	echo -e "$options_help"
	exit 1
}


#### set defalt arguments
broad_peak=0
macs2_pileup=1
macs2_qval=0.05
macs2_pval=-1
macs2_onlyMapping=0
randomN=0
randomNinput=0
use_cutadapt=1
input_judge=0

# read the options
TEMP=`getopt -o t:c:g:q:p:BNMd:D:Ah --long test-file:,control-file:,genome:,qvalue:,pvalue:,broad,nobdg,onlymapping,downsampleN:,downsampleNinput:,nocutadapt,help -n 'bior_chipseq_pipe.sh' -- "$@"`
eval set -- "$TEMP"


#### passing arguments
while :
do
    case "$1" in
    	-t | --test-file)
    			case "$2" in
    					"") shift 2;;
      				    *) test_file="$2"; shift 2;;
      		esac ;;
    	-c | --control-file)
    			case "$2" in
    					"") shift 2;;
    					*) control_file="$2"; shift 2;;
    			esac;;
	  	-g | --genome)
	  			case "$2" in
    					"") shift 2;;
    					*) annotation="$2"; shift 2;;
    			esac;;
		-q | --qvalue)
				case "$2" in
						"") shift 2;;
						*) macs2_qval="$2"; shift 2;;
				esac;;
		-p | --pvalue)
				case "$2" in
						"") shift 2;;
						*) macs2_pval="$2"; shift 2;;
				esac;;
	  	-B | --broad)
	  			broad_peak=1
	  			shift
	  			;;
		-N | --nobdg)
				macs2_pileup=0
				shift
				;;
		-M | --onlymapping)
				macs2_onlyMapping=1
				shift
				;;
		-d | --downsampleN)
				case "$2" in
						"") shift 2;;
						*) randomN="$2"; shift 2;;
				esac;;
		-D | --downsampleNinput)
				case "$2" in
						"") shift 2;;
						*) randomNinput="$2"; shift 2;;
				esac;;
		-A | --nocutadapt)
				use_cutadapt=0
				shift
				;;
		-h | --help)
	  			usage
	  			exit 0
	  			;;
	  	--) # End of all options
	  			shift
	  			break
	  			;;
	  	-*)
	  			echo "Error: Unknown option: $1" >&2
	  			exit 1
	  			;;
	  	*)  # No more options
      		exit 1
	  			;;
    esac
done


#### judge parameters
test_file_array=($test_file)
for i in ${!test_file_array[@]}; do
    if [ -z ${test_file_array[i]} ]; then
    	input_judge=1
    fi
done

control_file_array=($control_file)
for i in ${!control_file_array[@]}; do
    if [ -z ${control_file_array[i]} ]; then
    	input_judge=1
    fi
done

if [ -z $annotation ] || [ $input_judge == 1 ]; then
	usage
	exit 1
fi


#### set all parameters from command line options
if [ ! -z ${test_file_array[1]} ]; then
	testdir=${test_file_array[0]/_R1.fastq.gz/}
	testdir=${testdir/_R2.fq.gz/}
else
	testdir=${test_file/.fastq.gz/}
	testdir=${testdir/.fq.gz/}
fi

testbase=`basename $testdir`
testfull=$testdir/$testbase

if [ ! -z ${control_file_array[1]} ]; then
	controldir=${control_file_array[0]/_R1.fastq.gz/}
	controldir=${controldir/_R2.fq.gz/}
else
	controldir=${control_file/.fastq.gz/}
	controldir=${controldir/.fq.gz/}
fi

controlbase=`basename $controldir`
controlfull=$controldir/$controlbase

inputds=''


#### mv fastq files to folder
if [ ! -d $testdir ]; then
	mkdir $testdir
	mv $test_file $testdir
fi
if [ ! -d $controldir ]; then
	mkdir $controldir
	mv $control_file $controldir
fi


#### set genome version and annotation version
source $bdir/bior_chipseq_set_genome_annotation.sh


#### mapping with bowtie2
cd $testdir
if [ ! -e ${testbase}.rmdup.bam ]
then
	if [ $use_cutadapt == 1 ]
	then
		bior_chipseq_map_bowtie2.sh $test_file $annotation
	else
		bior_chipseq_map_bowtie2_simple.sh $test_file $annotation
	fi
fi
cd -

cd $controldir
if [ ! -e ${controlbase}.rmdup.bam ]
then
	if [ $use_cutadapt == 1 ]
	then
		bior_chipseq_map_bowtie2.sh $control_file $annotation
	else
		bior_chipseq_map_bowtie2_simple.sh $control_file $annotation
	fi
fi
cd -


#### exit if -M is true
if [ $macs2_onlyMapping == 1 ]
then
	echo ">>> Mapping is done for $test_file and $control_file!"
	exit 1
fi


#### down sample to N reads
if [ $randomN -gt 0 ]
then
	testds=${testfull}.rmdup.ds$randomN.bed
	if [ ! -e $testds.gz ]
	then
		echo ">>> Start to down sample TEST to $randomN reads..."
		bior_chipseq_downsample.sh $testfull $randomN
		gzip $testds
	else
		echo "(*_*) <$testds.gz> already exists...Skip~~"
	fi

	if [ $randomNinput -gt 0 ]
	then
		inputds=${controlfull}.rmdup.ds$randomNinput.bed
		if [ ! -e $inputds.gz ]
		then
			echo ">>> Start to down sample INPUT to $randomNinput reads..."
			bior_chipseq_downsample.sh $controlfull $randomNinput
			gzip $inputds
		else
			echo "(*_*) <$inputds.gz> already exists...Skip~~"
		fi
	else
		inputds=${controlfull}.rmdup.ds$randomN.bed
		if [ ! -e $inputds.gz ]
		then
			echo ">>> Start to down sample INPUT to $randomN reads..."
			bior_chipseq_downsample.sh $controlfull $randomN
			gzip $inputds
		else
			echo "(*_*) <$inputds.gz> already exists...Skip~~"
		fi
	fi
fi


#### define output names
outname=${testfull}_ds${randomN}

#### peak calling
if [ $randomN -gt 0 ]
then
	if [ $macs2_pval == -1 ]
	then
		macs2_callpeak_parP="-q $macs2_qval"
	else
		macs2_callpeak_parP="-p $macs2_pval"
	fi
	if [ $macs2_pileup == 1 ]
	then
		macs2_callpeak_par1="-B --keep-dup=all --extsize=146 --nomodel"
		macs2_callpeak_par2="-B --keep-dup=all --extsize=146 --nomodel --broad"
	else
		macs2_callpeak_par1="--keep-dup=all --extsize=146 --nomodel"
		macs2_callpeak_par2="--keep-dup=all --extsize=146 --nomodel --broad"
	fi
	if [ $broad_peak == 0 ]
	then
		if [ ! -e ${outname}_peaks.xls ]
		then
			macs2 callpeak $macs2_callpeak_par1 $macs2_callpeak_parP -g $macs_g -t $testds.gz -c $inputds.gz -n ${outname}
		else
			echo "(*_*) <${outname}_peaks.xls> already exists...Skip~~"
		fi
	else
		if [ ! -e ${outname}.broad_peaks.xls ]
		then
			macs2 callpeak $macs2_callpeak_par2 $macs2_callpeak_parP -g $macs_g -t $testds.gz -c $inputds.gz -n ${outname}.broad
		else
			echo "(*_*) <${outname}.broad_peaks.xls> already exists...Skip~~"
		fi
	fi
else
	if [ $macs2_pval == -1 ]
	then
		macs2_callpeak_parP="-q $macs2_qval"
	else
		macs2_callpeak_parP="-p $macs2_pval"
	fi
	if [ $macs2_pileup == 1 ]
	then
		macs2_callpeak_par1="--SPMR -B --keep-dup=all --extsize=146 --nomodel"
		macs2_callpeak_par2="--SPMR -B --keep-dup=all --extsize=146 --nomodel --broad"
	else
		macs2_callpeak_par1="--keep-dup=all --extsize=146 --nomodel"
		macs2_callpeak_par2="--keep-dup=all --extsize=146 --nomodel --broad"
	fi
	if [ $broad_peak == 0 ]
	then
		if [ ! -e ${outname}_peaks.xls ]
		then
			macs2 callpeak $macs2_callpeak_par1 $macs2_callpeak_parP -g $macs_g -t ${testfull}.rmdup.bam -c ${controlfull}.rmdup.bam -n ${outname}
		else
			echo "(*_*) <${outname}_peaks.xls> already exists...Skip~~"
		fi
	else
		if [ ! -e ${outname}.broad_peaks.xls ]
		then
			macs2 callpeak $macs2_callpeak_par2 $macs2_callpeak_parP -g $macs_g -t ${testfull}.rmdup.bam -c ${controlfull}.rmdup.bam -n ${outname}.broad
		else
			echo "(*_*) <${outname}.broad_peaks.xls> already exists...Skip~~"
		fi
	fi
fi


#### rename bdg files for broad calling
if [ -e ${outname}.broad_treat_pileup.bdg ] && [ ! -e ${outname}_treat_pileup.bdg ]
then
	mv ${outname}.broad_treat_pileup.bdg ${outname}_treat_pileup.bdg
fi

if [ -e ${outname}.broad_control_lambda.bdg ] && [ ! -e ${outname}_control_lambda.bdg ]
then
	mv ${outname}.broad_control_lambda.bdg ${outname}_control_lambda.bdg
fi


#### bedgraph2bigwiggle for test and input samples
if [ ! -e ${outname}.bw ]
then
		bedClip ${outname}_treat_pileup.bdg $chromlen ${outname}_treat_pileup.bdg.tmp
		bedSort ${outname}_treat_pileup.bdg.tmp ${outname}_treat_pileup.bdg.tmp
		bedGraphToBigWig ${outname}_treat_pileup.bdg.tmp $chromlen ${outname}_treat_pileup.bw
		mv ${outname}_treat_pileup.bw ${outname}.bw
		rm ${outname}_treat_pileup.bdg.tmp
else
		echo "(*_*) <${outname}_treat_pileup.bw> already exists...Skip~~"
fi

if [ ! -e ${controlfull}.bw ]
then
		bedClip ${outname}_control_lambda.bdg $chromlen ${outname}_control_lambda.bdg.tmp
		bedSort ${outname}_control_lambda.bdg.tmp ${outname}_control_lambda.bdg.tmp
		bedGraphToBigWig ${outname}_control_lambda.bdg.tmp $chromlen ${outname}_control_lambda.bw
		mv ${outname}_control_lambda.bw ${controlfull}.bw
		rm ${outname}_control_lambda.bdg.tmp
else
		echo "(*_*) <${outname}_control_lambda.bw> already exists...Skip~~"
fi


#### clear tmp files
if [ -e ${testfull}.bam ]; then
	rm ${testfull}.bam
fi

if [ -e ${controlfull}.bam ]; then
	rm ${controlfull}.bam
fi

if [ -e ${outname}_treat_pileup.bdg ]; then
	rm ${outname}_treat_pileup.bdg
fi

if [ -e ${outname}_control_lambda.bdg ]; then
	rm ${outname}_control_lambda.bdg
fi

if [ -e dup.txt ]; then
	rm dup.txt
fi







