#!/bin/bash
##
# Set genome version and annotation version
##

#### define genome and annotation version
if [ $annotation == "hg19_refseq" ]
then
	index="$bdir/ref/hg19/hg19_genome_bt2/hg19"
	chrom="$bdir/ref/hg19/hg19_genome_bt2/hg19.fa"
	gtf="$bdir/ref/hg19/hg19_refgene.gtf"
	gtfindex="$bdir/ref/hg19/hg19_refgene_bt2/hg19_refgene"
	gtfrsem="$bdir/ref/hg19/hg19_refgene_rsem/hg19_refgene_rsem"
	rrna="$bdir/ref/rRNA/human_all_rRNA.fasta"
	chromlen="$bdir/ref/hg19/hg19.genome"
	ceas_db="$bdir/CEAS/gdb/hg19_refgene.sql"
	tss="$bdir/ref/hg19/hg19_refgene_tss.bed"
	macs_g="hs"
elif [ $annotation == "hg19_gencode19" ]
then
	index="$bdir/ref/hg19/hg19_genome_bt2/hg19"
	chrom="$bdir/ref/hg19/hg19_genome_bt2/hg19.fa"
	gtf="$bdir/ref/hg19/hg19_gencode19.gtf"
	anno="$bdir/ref/hg19/hg19_gencode19.anno"
	gtfindex="$bdir/ref/hg19/hg19_gencode19_bt2/hg19_gencode19"
	gtfrsem="$bdir/ref/hg19/hg19_gencode19_rsem/hg19_gencode19_rsem"
	rrna="$bdir/ref/rRNA/human_all_rRNA.fasta"
	chromlen="$bdir/ref/hg19/hg19.genome"
	ceas_db="$bdir/CEAS/gdb/hg19_refgene.sql"
	tss="$bdir/ref/hg19/hg19_refgene_tss.bed"
	macs_g="hs"
elif [ $annotation == "mm10_gencodeM3" ]
then
	index="$bdir/ref/mm10/mm10_genome_bt2/GRCm38p3"
	chrom="$bdir/ref/mm10/mm10_genome_bt2/GRCm38p3.fa"
	gtf="$bdir/ref/mm10/mm10_gencodeM3.gtf"
	anno="$bdir/ref/mm10/mm10_gencodeM3.anno"
	chromlen="$bdir/ref/mm10/mm10_genome_bt2/GRCm38p3.sizes"
	gtfindex="$bdir/ref/mm10/mm10_gencodeM3_bt2/mm10_gencodeM3"
	gtfrsem="$bdir/ref/mm10/mm10_gencodeM3_rsem/mm10_gencodeM3_rsem"
	rrna="$bdir/ref/rRNA/mouse_all_rRNA.fasta"
	macs_g="mm"
elif [ $annotation == "dm6" ]
then
	index="$bdir/ref/dm6/dm6"
	chrom="$bdir/ref/dm6/dm6.fa"
	macs_g="dm"
	chromlen="$bdir/ref/dm6/dm6.genome"
fi


