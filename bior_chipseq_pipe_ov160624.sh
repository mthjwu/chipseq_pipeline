#!/bin/bash
####
# chipseq analysis pipeline
####

#### set path of the pipeline
bdir=`dirname $0`
ceas=$bdir/CEAS/bin

#### useage
red='\033[0;31m'
blue='\033[1;34m'
green='\033[1;32m'
NC='\033[0m' # No Color
options_help="
${red}Command line${NC}: ${green}
    bior_chipseq_pipe.sh [options] [-t <test_fastq_file>] [-c <control_fastq_file>] [-g <genome annotation version: hg19_refseq,hg19_gencode19,mm10_gencodeM3>]

${red}Options${NC}: ${green}
	-q | --qvalue:             q-value cutoff for macs2
	-p | --pvalue:             p-value cutoff for macs2 (if this is set, qvalue will be not used)
	-B | --broad:              Broad peak calling with Macs2
	-N | --nobdg:              Don't allow macs2 to output normalized bdg file
	-M | --onlymapping:        Only do preprocessing and mapping
	-d | --downsampleN:        Down sample bam file to N tags for both test and input, if -D isn't set (default don't down sample)
	-D | --downsampleNinput:   Down sample bam file to N tags for input sample (default don't down sample)
	-h | --help:               help
${NC}
"
usage () {
	echo -e "\n${red}Usage${NC}: ${green}`basename $0` -h for help${NC}"
	echo -e "$options_help"
	exit 1
}


#### set defalt arguments
broad_peak=0
macs2_pileup=1
macs2_qval=0.05
macs2_pval=-1
macs2_onlyMapping=0
randomN=0
randomNinput=0

# read the options
TEMP=`getopt -o t:c:g:q:p:BNMd:D:h --long test-file:,control-file:,genome:,qvalue:,pvalue:,broad,nobdg,onlymapping,downsampleN:,downsampleNinput:,help -n 'bior_chipseq_pipe.sh' -- "$@"`
eval set -- "$TEMP"


#### passing arguments
while :
do
    case "$1" in
    	-t | --test-file)
    			case "$2" in
    					"") shift 2;;
      				    *) test_file="$2"; shift 2;;
      		esac ;;
    	-c | --control-file)
    			case "$2" in
    					"") shift 2;;
    					*) control_file="$2"; shift 2;;
    			esac;;
	  	-g | --genome)
	  			case "$2" in
    					"") shift 2;;
    					*) annotation="$2"; shift 2;;
    			esac;;
		-q | --qvalue)
				case "$2" in
						"") shift 2;;
						*) macs2_qval="$2"; shift 2;;
				esac;;
		-p | --pvalue)
				case "$2" in
						"") shift 2;;
						*) macs2_pval="$2"; shift 2;;
				esac;;
	  	-B | --broad)
	  			broad_peak=1
	  			shift
	  			;;
		-N | --nobdg)
				macs2_pileup=0
				shift
				;;
		-M | --onlymapping)
				macs2_onlyMapping=1
				shift
				;;
		-d | --downsampleN)
				case "$2" in
						"") shift 2;;
						*) randomN="$2"; shift 2;;
				esac;;
		-D | --downsampleNinput)
				case "$2" in
						"") shift 2;;
						*) randomNinput="$2"; shift 2;;
				esac;;
		-h | --help)
	  			usage
	  			exit 0
	  			;;
	  	--) # End of all options
	  			shift
	  			break
	  			;;
	  	-*)
	  			echo "Error: Unknown option: $1" >&2
	  			exit 1
	  			;;
	  	*)  # No more options
      		exit 1
	  			;;
    esac
done


#### judge parameters
if [ -z $test_file ] || [ -z $control_file ] || [ -z $annotation ]
then
	usage
	exit 1
fi


#### set all parameters from command line options
sampledir=`dirname $test_file`
testname=`basename $test_file ".fq.gz"`
testname=`basename $testname ".fastq.gz"`
controlname=`basename $control_file ".fq.gz"`
controlname=`basename $controlname ".fastq.gz"`
inputds=''


#### set genome version and annotation version
source $bdir/bior_chipseq_set_genome_annotation.sh


#### mapping with bowtie2
bior_chipseq_map_bowtie2.sh $test_file $annotation
bior_chipseq_map_bowtie2.sh $control_file $annotation


#### exit if -M is true
if [ $macs2_onlyMapping == 1 ]
then
	echo ">>> Mapping is done for $test_file and $control_file!"
	exit 1
fi


#### down sample to N reads
if [ $randomN -gt 0 ]
then
	if [ ! -e $testname.rmdup.ds$randomN.bed ]
	then
		echo ">>> Start to down sample TEST to $randomN reads..."
		bior_chipseq_downsample.sh $testname $randomN
	else
		echo "(*_*) <$testname.rmdup.ds$randomN.bed> already exists...Skip~~"
	fi

	if [ $randomNinput -gt 0 ]
	then
		inputds=$controlname.rmdup.ds$randomNinput.bed
		if [ ! -e $inputds ]
		then
			echo ">>> Start to down sample INPUT to $randomNinput reads..."
			bior_chipseq_downsample.sh $controlname $randomNinput
		else
			echo "(*_*) <$inputds> already exists...Skip~~"
		fi
	else
		inputds=$controlname.rmdup.ds$randomN.bed
		if [ ! -e $inputds ]
		then
			echo ">>> Start to down sample INPUT to $randomN reads..."
			bior_chipseq_downsample.sh $controlname $randomN
		else
			echo "(*_*) <$inputds> already exists...Skip~~"
		fi
	fi
fi


#### peak calling
if [ $randomN -gt 0 ]
then
	if [ $macs2_pval == -1 ]
	then
		macs2_callpeak_parP="-q $macs2_qval"
	else
		macs2_callpeak_parP="-p $macs2_pval"
	fi
	if [ $macs2_pileup == 1 ]
	then
		macs2_callpeak_par1="-B --keep-dup=all --extsize=146 --nomodel"
		macs2_callpeak_par2="-B --keep-dup=all --extsize=146 --nomodel --broad"
	else
		macs2_callpeak_par1="--keep-dup=all --extsize=146 --nomodel"
		macs2_callpeak_par2="--keep-dup=all --extsize=146 --nomodel --broad"
	fi
	if [ $broad_peak == 0 ]
	then
		if [ ! -e ${testname}_peaks.xls ]
		then
			macs2 callpeak $macs2_callpeak_par1 $macs2_callpeak_parP -g $macs_g -t $testname.rmdup.ds$randomN.bed -c $inputds -n $testname
		else
			echo "(*_*) <${testname}_peaks.xls> already exists...Skip~~"
		fi
	else
		if [ ! -e ${testname}.broad_peaks.xls ]
		then
			macs2 callpeak $macs2_callpeak_par2 $macs2_callpeak_parP -g $macs_g -t $testname.rmdup.ds$randomN.bed -c $inputds -n $testname.broad
		else
			echo "(*_*) <${testname}.broad_peaks.xls> already exists...Skip~~"
		fi
	fi
else
	if [ $macs2_pval == -1 ]
	then
		macs2_callpeak_parP="-q $macs2_qval"
	else
		macs2_callpeak_parP="-p $macs2_pval"
	fi
	if [ $macs2_pileup == 1 ]
	then
		macs2_callpeak_par1="--SPMR -B --keep-dup=all --extsize=146 --nomodel"
		macs2_callpeak_par2="--SPMR -B --keep-dup=all --extsize=146 --nomodel --broad"
	else
		macs2_callpeak_par1="--keep-dup=all --extsize=146 --nomodel"
		macs2_callpeak_par2="--keep-dup=all --extsize=146 --nomodel --broad"
	fi
	if [ $broad_peak == 0 ]
	then
		if [ ! -e ${testname}_peaks.xls ]
		then
			macs2 callpeak $macs2_callpeak_par1 $macs2_callpeak_parP -g $macs_g -t $testname.rmdup.bam -c $controlname.rmdup.bam -n $testname
		else
			echo "(*_*) <${testname}_peaks.xls> already exists...Skip~~"
		fi
	else
		if [ ! -e ${testname}.broad_peaks.xls ]
		then
			macs2 callpeak $macs2_callpeak_par2 $macs2_callpeak_parP -g $macs_g -t $testname.rmdup.bam -c $controlname.rmdup.bam -n $testname.broad
		else
			echo "(*_*) <${testname}.broad_peaks.xls> already exists...Skip~~"
		fi
	fi
fi


#### bedgraph2bigwiggle
if [ ! -e ${testname}_treat_pileup.bw ]
then
		bedClip ${testname}_treat_pileup.bdg $chromlen $$
		bedGraphToBigWig $$ $chromlen ${testname}_treat_pileup.bw
		rm $$
		bedClip ${testname}_control_lambda.bdg $chromlen $$
		bedGraphToBigWig $$ $chromlen ${testname}_control_lambda.bw
		rm $$
else
		echo "(*_*) <${testname}_treat_pileup.bw> already exists...Skip~~"
fi


# #### fold enrichment calculation and bedgraph2bigwiggle
# if [ ! -e ${testname}_FE.bw ]
# then
# 		macs2 bdgcmp -t ${testname}_treat_pileup.bdg -c ${testname}_control_lambda.bdg -o ${testname}_FE.bdg -m FE
# 		bedClip ${testname}_FE.bdg $chromlen $$
# 		bedGraphToBigWig $$ $chromlen ${testname}_FE.bw
# 		rm $$
# else
# 		echo "(*_*) <${testname}_FE.bw> already exists...Skip~~"
# fi


# #### peak annotation
# if [ ! -e ${testname}_peaks.narrowPeak.anno ]
# then
# 	annotatePeaks.pl ${testname}_peaks.narrowPeak hg19 > ${testname}_peaks.narrowPeak.anno
# else
# 	echo "(*_*) <${testname}_peaks.narrowPeak.anno> already exists...Skip~~"
# fi

# #### CEAS
# if [ ! -e ${testname}.ceas.xls ]
# then
#     $ceas/ceasBW -w ${testname}_treat_pileup.bw -b ${testname}_summits.bed -g $ceas_db -l $chromlen --dump --name=${testname}.ceas --bg
# else
#     echo "(*_*) <${testname}.ceas.xls> already exists...Skip~~"
# fi

# #### parsing CEAS output
# if [ ! -e ${testname}.ceas.piedata ]
# then
#     grep "^pie(" $testname.ceas.R |sed 's/.*labels=c("//g' |sed 's/ %"),main=.*//g' |sed 's/%","//g' |awk -v tag=$testname 'NR==2 {print "Sample\tPromoter_up3k\tDownstream_dn3k\t5\x27UTR\t3\x27UTR\tCodingExon\tIntron\tDistalIntergenic\n"tag"\t"$1+$2+$3"\t"$4+$5+$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11}' > $testname.ceas.piedata
# else
# 	echo "(*_*) <${testname}.ceas.piedata> already exists...Skip~~"
# fi


# #### TSS profile
# #if [ ! -e ${testname}.siteproBW.tss_dump.txt ]
# #then
# #    siteproBW -w ${testname}_treat_pileup.bw -b $tss --dump --name=${testname}.siteproBW.tss -l ${testname}.siteproBW.tss
# #else
# #    echo "(*_*) <${testname}.siteproBW.tss_dump.txt> already exists...Skip~~"
# #fi


# #### peak summits profile
# if [ ! -e ${testname}.siteproBW.summits_dump.txt ]
# then
#     siteproBW -w ${testname}_treat_pileup.bw -b ${testname}_summits.bed --span=3000 --dump --name=${testname}.siteproBW.summits -l ${testname}.siteproBW.summits
# else
#     echo "(*_*) <${testname}.siteproBW.summits_dump.txt> already exists...Skip~~"
# fi



